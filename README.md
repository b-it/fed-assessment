# Readme

Main features:

-   Sass compilation + Post CSS autoprefixer
-   Babel ES6 conversion
-   ESLint
-   Hot-reload HTML, CSS and JS

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

# HTML/CSS knowledge check

**Box Model** is about considering HTML elements as boxes. Each box can have some content, size, margins, paddings, borders, position and other attributes applied, but there is one fundamental attribute we don't have impact on - the shape. Each web page's layout is built from various sets of 'boxes/containers' and the front end developers job is to give those boxes a functional purpose, to manipulate containers attributes and mutual location to achieve the expected result.

# JS exercise

```js
const sales = [
    {
        itemSold: "Football",
        price: 19.99,
        dateSold: "2018-04-07",
        id: "j_123",
    },
    {
        itemSold: "Trainers",
        price: 159.95,
        dateSold: "2018-03-02",
        id: "t_acds1",
    },
    {
        itemSold: "Cricket bat",
        price: 204.97,
        dateSold: "2018-04-05",
        id: "j_456",
    },
    {
        itemSold: "Rugby ball",
        price: 30.0,
        dateSold: "2017-04-22",
        id: "t_acds3",
    },
    {
        itemSold: "Hockey stick",
        price: 54.95,
        dateSold: "2017-03-19",
        id: "j_999",
    },
];
```

## Return the sum of the price of all properties as a single value.

```js
/**
 * Sums up all price properties for given aray of sales items
 *
 * @param {array}       sales  array of sales items
 *
 * @return {number}     sum of of all price properties
 */

const getSum = (sales = []) => {
    //tmp variables
    let prevDecimal = null;
    let currentValDecimal = null;

    //get the sum of all prices
    const sum = sales.reduce((prevVal, currentVal) => {
        prevDecimal = new Decimal(prevVal);
        currentValDecimal = new Decimal(currentVal.price || 0);

        return prevDecimal.plus(currentValDecimal);
    }, 0);

    return sum.toNumber(); // a number type should be returned
};

getSum(sales);
```

The `getSum` method requires https://github.com/MikeMcl/decimal.js-light library to fix floating-point issues while nubers are being added up.

## Return the items which were sold in 2017.

```js
/**
 * Get item by the year
 *
 * @param {array}       sales  array of sales items
 *
 * @return {array}      array of obejcts for given dateSold date
 */

const getItemByYear = (sales = [], year = null) => {
    return sales.filter((item) => {
        //parse date string to date obj
        const itemDate = new Date(item.dateSold);

        return itemDate.getFullYear() === parseInt(year);
    });
};

getItemByYear(sales, "2017");
```

## Return an array of all of the itemsSold properties as strings, sorted alphabetically.

```js
/**
 * Get comma separated string with names of sold items
 *
 * @param {array}       sales  array of sales items
 *
 * @return {string}     names of sold items
 */

const getSoldItemsNames = (sales = []) => {
    return sales
        .map((item) => {
            return item.itemSold || "N/A";
        })
        .sort()
        .join(", ");
};

getSoldItemsNames(sales);
```

## Using id as an argument, return the sale which matches the id.

```js
/**
 * Get comma separated string with names of sold items
 *
 * @param {array}       sales  array of sales items
 *
 * @return {object}     sales item for give id
 */

const getItemById = (sales = [], id = "") =>
    sales.find((item) => item.id === id);

getItemById(sales, "j_456");
```

Additionally, I've created a very basic user interface allowing the user to execute the above methods:
http://0.0.0.0:8080/js-assigment.html

# Replicate layout

The layout can be found under the local path http://0.0.0.0:8080/layout-exercise.html. I've given a go to 'less is more' approach ;) For that simple layout, I've put most of the scss in a single src/modules/\_global/css/global.scss file. I've used some functions and variables which came with the boilerplate. For search I have created a separate module ( src/modules/search-bar/), so the bar can be easily reused in other areas if needed (e.g. footer) The template should meet AA WCAG conformance. To support older browsers the Post CSS autoprefixer has been applied.
